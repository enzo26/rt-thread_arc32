# the name of the target operating system
set(CMAKE_SYSTEM_NAME      Generic)
set(CMAKE_SYSTEM_VERSION   1)
set(CMAKE_SYSTEM_PROCESSOR ARC)
cmake_minimum_required(VERSION 3.14)

# 选择TOOLCHAIN,MWDT或者GNU编译工具
set(TOOLCHAIN_GNU  true)
#set(TOOLCHAIN_MWDT true)

# 避免cmake去生成默认测试文件检查编译器是否工作
set (CMAKE_C_COMPILER_WORKS 1)
set (CMAKE_CXX_COMPILER_WORKS 1)

# which compilers to use for C and C++
if(TOOLCHAIN_MWDT)
    set(EXE_SUFFIX   ".exe")
    # 如果加入到系统环境变量，不需要指定TOOLCHAIN_PATH
    #set(TOOLCHAIN_PATH  "D:/arc_gnu/bin/")
    set(CMAKE_ASM_COMPILER  ccac)
    set(CMAKE_C_COMPILER  ccac)
    set(CMAKE_C_COMPILE_AR  ccar)
    set(CMAKE_CXX_COMPILER  ccac)
    set(CMAKE_CXX_COMPILER_AR ccar)
    set(OBJCOPY elf2bin)
    set(OBJDUMP elfdump)
    set(ELF2HEX elf2hex)
    message("toolchain: MWDT ")

elseif(TOOLCHAIN_GNU)
    if(WIN32)
        set(TOOLCHAIN_PATH  "D:/arc_gnu/bin/")
        set(CROSS_COMPILE_PREFIX  "arc-elf32-")
        set(EXE_SUFFIX   ".exe")
        set(CMAKE_LIBRARY_PATH "D:/arc_gnu/arc-elf32/lib/em4_fpuda")
        include_directories("D:/arc_gnu/arc-elf32/include")
    else()
        set(EXE_SUFFIX   "")
        set(CROSS_COMPILE_PREFIX  "arc-elf32-")
        set(TOOLCHAIN_PATH  "/opt/arc_gnu_202003/bin/")
        set(CMAKE_LIBRARY_PATH "/opt/arc_gnu_202003/arc-elf32/lib/em4_fpuda")
        include_directories("/opt/arc_gnu_202003/arc-elf32/include")
    endif()
# Specify C, C++ and ASM compilers
    set(CMAKE_C_COMPILER    ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}gcc${EXE_SUFFIX})
    set(CMAKE_CXX_COMPILER  ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}g++${EXE_SUFFIX})
    set(AS                  ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}as${EXE_SUFFIX})
    set(AR                  ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}ar${EXE_SUFFIX})
    set(OBJCOPY             ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}objcopy${EXE_SUFFIX})
    set(OBJDUMP             ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}objdump${EXE_SUFFIX})
    set(SIZE                ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}size${EXE_SUFFIX})
    set(GDB                 ${TOOLCHAIN_PATH}${CROSS_COMPILE_PREFIX}gdb${EXE_SUFFIX})
endif()


message("toolchain: GNU ")
message(${CMAKE_LIBRARY_PATH})

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
# core flags
if(TOOLCHAIN_MWDT)
    add_definitions(-D__MW__)
    set(CORE_FLAGS "-arcv2em -core4 -Hrgf_banked_regs=8 -HL -Xunaligned -Xcode_density -Xdiv_rem=radix4 -Xswap -Xbitscan -Xmpy_option=wlh2 \
    -Xshift_assist -Xbarrel_shifter -Xfpus_div -Xfpu_mac -Xfpuda -Xfpus_mpy_fast -Xfpus_div_slow -Xtimer0 -Xtimer1 -Xrtc -Xstack_check \
    -Hccm -Xdmac -Hpc_width=32 -Hrgf_banked_regs=8 -Hnosdata -Hnocopyr ${ADDITIONAL_CORE_FLAGS}")
    # -Hasopt=-g -Hasmcpp -Hnocopyr
    set(CMAKE_ASM_FLAGS "${CORE_FLAGS} -g -Hasopt=-g -Hasmcpp  " CACHE INTERNAL "asm compiler flags")
    set(CMAKE_ASM_FLAGS_DEBUG "" CACHE INTERNAL "asm compiler flags: Debug")
    set(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm compiler flags: Release")

elseif(TOOLCHAIN_GNU)

    add_definitions(-D__GNU__)
    #-fdebug-prefix-map=${PROJECT_SOURCE_DIR}=.
    set(CORE_FLAGS   "@${PROJECT_SOURCE_DIR}/bsp/taurus/configs/gcc.arg -g3 -gdwarf-2 -mno-sdata -MMD -MT -MF  ${ADDITIONAL_CORE_FLAGS}")
    message(${CORE_FLAGS})
    set(CMAKE_ASM_FLAGS "@${PROJECT_SOURCE_DIR}/bsp/taurus/configs/gcc.arg  -x assembler-with-cpp ")
    set(CMAKE_EXE_LINKER_FLAGS "-nostartfiles ")
endif()

# compiler: language specific flags
set(CMAKE_C_FLAGS "${CORE_FLAGS}   " CACHE INTERNAL "c compiler flags")
set(CMAKE_C_FLAGS_DEBUG "${CORE_FLAGS}   -O0" CACHE INTERNAL "c compiler flags: Debug")
set(CMAKE_C_FLAGS_RELEASE "${CORE_FLAGS} -Os  " CACHE INTERNAL "c compiler flags: Release")

set(CMAKE_CXX_FLAGS "${CORE_FLAGS}  " CACHE INTERNAL "cxx compiler flags")
set(CMAKE_CXX_FLAGS_DEBUG "${CORE_FLAGS} " CACHE INTERNAL "cxx compiler flags: Debug")
set(CMAKE_CXX_FLAGS_RELEASE "${CORE_FLAGS} " CACHE INTERNAL "cxx compiler flags: Release")

#disable c++ featrue
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-inline")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-exceptions")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-rtti")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ffunction-sections -fdata-sections")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ffunction-sections -fdata-sections")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}  -Wl,--gc-sections,--print-memory-usage")


# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# find additional toolchain executables
if(TOOLCHAIN_MWDT)
    find_program(ARC_SIZE_EXECUTABLE sizeac)
    find_program(ARC_MDB mdb)
    find_program(ARC_OBJCOPY_EXECUTABLE elf2bin)
    find_program(ARC_OBJDUMP_EXECUTABLE elfdump)
elseif(TOOLCHAIN_GNU)

endif()

