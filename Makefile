debug:
	cd build && cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_VERBOSE_MAKEFILE=ON -D"CMAKE_TOOLCHAIN_FILE=../arc_toolchain.cmake" ../ && \
make -j4
clean:
	rm -rf build/*
openocd:
	openocd -s ~/arc_gnu_2020.03_ide_linux_install/share/openocd/scripts/ -f board/taurus.cfg
cgdb:
	cgdb -d arc-elf32-gdb build/taurus_radar.elf
