/*
 * printf_config.h
 *
 *  Created on:
 *      Author: wdxc-39
 */

#ifndef APP_PRINTF_CONFIG_H_
#define APP_PRINTF_CONFIG_H_

#define PRINTF_DISABLE_SUPPORT_FLOAT				//Define this to disable floating point (%f) support
#define PRINTF_DISABLE_SUPPORT_EXPONENTIAL			//Define this to disable exponential floating point (%e) support
#define PRINTF_DISABLE_SUPPORT_LONG_LONG			//Define this to disable long long (%ll) support
#define PRINTF_DISABLE_SUPPORT_PTRDIFF_T			//Define this to disable ptrdiff_t (%t) support

#endif /* APP_PRINTF_CONFIG_H_ */
