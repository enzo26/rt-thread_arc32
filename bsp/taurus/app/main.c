/*
 * Copyright (c) 2018, Synopsys, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <rtthread.h>
#include "emsk.h"
#include <math.h>
#include <ringbuffer.h>
#include <string.h>
#include "embARC.h"



#define LOG_LVL  LOG_LVL_DBG
#define LOG_TAG  "[TEST]"
#include "ulog.h"

#define   FIX_TASK_OVERFLOW                    (1)
#define MAX_BUFF_SIZE    (64)
#define SWI_INTNO        (20)

void perf_start(void);
uint32_t perf_end(void);
void perf_init(uint32_t id);

static uint32_t perf_id = 0xff;

static uint32_t perf_start_time=0;

static  rt_thread_t tid2, tid1;

static rt_mutex_t  mut1;
static rt_sem_t  sem1;
static rt_mailbox_t  mb1;
static rt_mq_t mq1;

static rt_event_t  evt1;

static rt_sem_t task_done_sem;

typedef struct _sample{
    uint32_t t_measure;
    uint32_t t_t2_t1;
    uint32_t t_int_t1;
    uint32_t t_t1_t2;
    uint32_t t_t1_int;
    uint32_t t_int_nest;
    uint32_t t_nest_int;
    uint32_t mux_rls;
    uint32_t t_mut_send;
    uint32_t t_sem_send;
    //消息邮箱
    uint32_t t_mb_send;
    //消息队列
    uint32_t t_mq_send;
    //event
    uint32_t t_evt_send;

}bench_sample_t;

bench_sample_t g_bench;

typedef struct {
    uint32_t buff[MAX_BUFF_SIZE];
    uint32_t put_index;
    uint32_t get_index;
    uint32_t capacity;
    rt_mutex_t  lock;
    rt_sem_t  full_sem;
    rt_sem_t empty_sem;

}trans_buff_t;



static uint32_t trans_buff_next(trans_buff_t *buff, uint32_t i){
    if((i+1) >= buff->capacity){
        i = 0;

    }else{
        i += 1;
    }

    return i;
}

static void trap_exception(void *p_excinf)
{
    g_bench.t_int_nest = perf_end();
    perf_start();

}

static void soft_interrupt(void *p_excinf)
{
    //2-E
    g_bench.t_t1_int = perf_end();
    perf_start();
//    Asm("trap_s 1");
    g_bench.t_nest_int = perf_end();
    //5-S Int->Task1
    arc_aux_write(AUX_IRQ_HINT, 0);// 清软件中断
    perf_start();

}

void task2(void *param)
{
    static uint32_t perf_times= 0;
    uint32_t mb_send_val=23;
    perf_init(TIMER_1);
    uint8_t msg_recv[256];

    while(1){

        //挂起task2
//        rt_kprintf("task2 suspend\r\n");
        //task2->task1 start

        rt_thread_suspend(tid2);
        perf_start();
        rt_schedule();
//        rt_thread_yield();
//        rt_kprintf("task2 resume\r\n");
// 6-End
        g_bench.t_t1_t2 = perf_end();

        //7-start
        perf_start();
        rt_mutex_release(mut1);

        //8-end
        rt_sem_take(sem1, RT_WAITING_FOREVER);
        g_bench.t_sem_send = perf_end();

        //9-start mailbox
        perf_start();
        rt_mb_send(mb1, mb_send_val);

        //10-end
        rt_mq_recv(mq1, msg_recv, 4, RT_WAITING_FOREVER);
        g_bench.t_mq_send =  perf_end();
        for(uint32_t i=0; i< 4; i++){
            dbg("%c\r\n", msg_recv[i]);
        }

        //11-start
        perf_start();
        rt_event_send(evt1, (1<<3));

        //等待task1完成
        rt_sem_take(task_done_sem, RT_WAITING_FOREVER);

        if(perf_times){
            rt_kprintf("task2->task1 : %d cycle\r\n", g_bench.t_t2_t1);
            rt_kprintf("task1->int : %d cycle\r\n", g_bench.t_t1_int);
            rt_kprintf("int->task1 : %d cycle\r\n", g_bench.t_int_t1);
            rt_kprintf("task1->task2 : %d cycle\r\n", g_bench.t_t1_t2);
            rt_kprintf("mutex send : %d cycle\r\n", g_bench.t_mut_send);
            rt_kprintf("sem send : %d cycle\r\n", g_bench.t_sem_send);
            rt_kprintf("mailbox send : %d cycle\r\n", g_bench.t_mb_send);
            rt_kprintf("mqueue send : %d cycle\r\n", g_bench.t_mq_send);
            rt_kprintf("event send : %d cycle\r\n", g_bench.t_evt_send);
        }

        perf_times++;

    }
}

void task1(void *param)
{
    uint32_t mb_recv_val;
    uint8_t *msg_send = "hello world";
    uint32_t e;
    while(1){
        //task2->task1
        g_bench.t_t2_t1 = perf_end();

        //task1->int
        perf_start();
        arc_aux_write(AUX_IRQ_HINT, SWI_INTNO);
        g_bench.t_int_t1 = perf_end();

        //6-S : task1->task2
        rt_thread_resume(tid2);
        perf_start();
        rt_schedule();

        //7-end mutex
        rt_mutex_take(mut1, RT_WAITING_FOREVER);
        g_bench.t_mut_send = perf_end();
        //8-start sem
        perf_start();
        rt_sem_release(sem1);

        //9-end
        rt_mb_recv(mb1, (uint32_t *)&mb_recv_val,RT_WAITING_FOREVER);
        g_bench.t_mb_send = perf_end();
        rt_kprintf("mb_val=%d\r\n", mb_recv_val);
        //10-start
        perf_start();
        rt_mq_send(mq1, msg_send, 4);
        //11-end, 接收完成后，清除标志位
        rt_event_recv(evt1, (1<<3), RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, RT_WAITING_FOREVER, &e);
        g_bench.t_evt_send = perf_end();
        dbg("e=0x%x\r\n", e);

        rt_sem_release(task_done_sem);
    }
}
/** @brief: 内核benchmark测试
 * 1. Task2 -> Task1
 * 2. Task1 -> Int, Task ->Fiq
 * 3. Int -> Task1
 * 4.
 *
 */

void os_bench()
{

    int_handler_install(SWI_INTNO, (EXC_HANDLER_T)soft_interrupt);
    int_enable(SWI_INTNO);

    exc_handler_install(EXC_NO_TRAP, trap_exception);

    if( (tid2 = rt_thread_create("task2", task2, RT_NULL, 768, 3, 100)) ){
        rt_kprintf("create task2 ok\r\n");

    }

    if( (tid1=rt_thread_create("task1", task1, RT_NULL, 1024, 5, 100)) ){
        rt_kprintf("create task1 ok\r\n");

    }



    //create mutex
    mut1 = rt_mutex_create("mut1", RT_IPC_FLAG_FIFO);

    //create sem
    sem1 = rt_sem_create("sem1", 0, RT_IPC_FLAG_FIFO);

    mb1 = rt_mb_create("mb1", 10, RT_IPC_FLAG_FIFO);
    if(mb1 == RT_NULL){
        rt_kprintf("create mb1 error\r\n");
    }

    mq1 = rt_mq_create("mq1", 1, 32, RT_IPC_FLAG_FIFO);

    evt1 = rt_event_create("evt1", RT_IPC_FLAG_FIFO);

    task_done_sem = rt_sem_create("task_done", 0, RT_IPC_FLAG_FIFO);
    rt_thread_startup(tid2);
    rt_thread_startup(tid1);
}

static uint32_t cnt=0;
static rt_timer_t timer1;

trans_buff_t trans_buff;

static void timeout1(void *param)
{
    rt_kprintf("%d:periodic timer is timeout %d\r\n", rt_tick_get(), cnt);

    if(cnt++>=9){
        rt_timer_stop(timer1);
        rt_kprintf("periodic timer was stopped\r\n");
    }
}

static uint32_t sem_cnt = 0;
void producer_thread(){

    while(sem_cnt < 20){

        //buffer不满
        rt_sem_take(trans_buff.empty_sem, RT_WAITING_FOREVER);;

        rt_mutex_take(trans_buff.lock, RT_WAITING_FOREVER);
        trans_buff.buff[trans_buff.put_index] = sem_cnt + 1;

        rt_kprintf("put: %d\r\n", trans_buff.buff[trans_buff.put_index] );
        //更新put_index
        trans_buff.put_index = trans_buff_next(&trans_buff, trans_buff.put_index);
        rt_sem_release(trans_buff.full_sem);
        sem_cnt += 1;

        rt_mutex_release(trans_buff.lock);

        rt_thread_mdelay(300);
    }
}

void consumer_thread(){

    while(1){
        //如果buffer为空，等待
        rt_sem_take(trans_buff.full_sem, RT_WAITING_FOREVER);
        //锁buffer
        rt_mutex_take(trans_buff.lock, RT_WAITING_FOREVER);
        //获取数据
        uint32_t e = trans_buff.buff[trans_buff.get_index];
        rt_kprintf("e=%d\r\n", e);
        trans_buff.get_index = trans_buff_next(&trans_buff, trans_buff.get_index);
        rt_sem_release(trans_buff.empty_sem);
        //释放锁
        rt_mutex_release(trans_buff.lock);
        rt_thread_mdelay(500);
    }
}

#define RING_BUFF_SIZE        (256)

static struct rt_ringbuffer *rb1;

static void ringbuffer_consumer(void *arg){

}

void test_ringbuff()
{
    uint8_t *put_str = "12345";
    uint8_t ch;
    uint32_t len;
    rb1 =  rt_ringbuffer_create(RING_BUFF_SIZE);
    if(rb1 == RT_NULL){
        rt_kprintf("Create ringbuffer error\r\n");
        return ;
    }
    rt_ringbuffer_put(rb1, put_str, strlen(put_str));

    while( len=rt_ringbuffer_data_len(rb1)) {
        rt_ringbuffer_getchar(rb1, &ch);
        rt_kprintf("%c\r\n", ch);
    }

}

void perf_init(uint32_t id)
{
    if (arc_timer_start(id, TIMER_CTRL_NH, 0xFFFFFFFF) < 0) {
        rt_kprintf("perf timer init failed\r\n");
        while(1);
    }
    perf_id = id;
}

/** performance timer perf_start */
void perf_start(void)
{
    if (arc_timer_current(perf_id, (void *)(&perf_start_time)) < 0) {
        perf_start_time = 0;
    }
}

/** performance timer end, and return the time passed */
uint32_t perf_end(void)
{
    uint32_t end = 0;

    if (arc_timer_current(perf_id, (void *)(&end)) < 0) {
        return 0;
    }

    if (perf_start_time < end) {
        return (end - perf_start_time);
    } else {
        return (0xFFFFFFFF - perf_start_time + end);
    }
}

void task_high(void *param)
{
    while(1){

        rt_thread_mdelay(100);
    }
}

void task_middle(void *param)
{
    static uint32_t cnt = 0;
    while(1){
        log_d("%s: %d\r\n", __func__, cnt++);
        rt_thread_mdelay(500);
    }
}


void task_low(void *param)
{
    static uint32_t cnt=0;
    while(1){
        log_d("%s: %d\r\n", __func__, cnt++);
        rt_thread_mdelay(500);
    }
}



uint32_t main(){
#if 0
    trans_buff.lock = rt_mutex_create("lock", 1);
    trans_buff.empty_sem = rt_sem_create("empty", MAX_BUFF_SIZE, RT_IPC_FLAG_FIFO);
    trans_buff.full_sem = rt_sem_create("full", 0, RT_IPC_FLAG_FIFO);
    trans_buff.get_index = trans_buff.put_index = 0;
    trans_buff.capacity = MAX_BUFF_SIZE;

    timer1 = rt_timer_create("timer1", timeout1, RT_NULL, 1000, RT_TIMER_FLAG_PERIODIC);
    if(timer1 != RT_NULL){
        rt_timer_start(timer1);
    }


    rt_thread_t producer = rt_thread_create("producer", producer_thread, RT_NULL,
                                1024, 3, 5);

    rt_thread_t consumer = rt_thread_create("consumer", consumer_thread, RT_NULL,
                                            1024, 3, 5);
    if(producer != RT_NULL){
        rt_thread_startup(producer);
    }

    if(consumer != RT_NULL){
        rt_thread_startup(consumer);
    }

#endif
#if 0
    float a = 2.3f * 2.3f;
    while(1){


        LOG_E("a=%.3f tail\r\n" ,  a);
//        LOG_E("a=%d tail\r\n" ,  1);
        rt_thread_mdelay(1000);
    }
#endif

#if FIX_TASK_OVERFLOW==1
    rt_thread_t low_tid = rt_thread_create("task_low", task_low, RT_NULL, 1024 * 8, 6, 20);
    if(low_tid){
        rt_thread_startup(low_tid);
    }

    rt_thread_t middle_tid = rt_thread_create("task_middle", task_middle, RT_NULL, 2048, 5, 20);
    if(middle_tid){
        rt_thread_startup(middle_tid);
    }
    rt_thread_t high_tid = rt_thread_create("task_high", task_high, RT_NULL, 1024 * 8, 2, 20);
    if(high_tid){
        rt_thread_startup(high_tid);
    }
#endif
    return 0;
}

#ifdef RT_USING_FINSH
MSH_CMD_EXPORT(test_ringbuff, test ring buffer);
MSH_CMD_EXPORT(os_bench, benchmark test);
#endif