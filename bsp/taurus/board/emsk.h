/* ------------------------------------------
 * Copyright (c) 2017, Synopsys, Inc. All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:

 * 1) Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * 2) Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.

 * 3) Neither the name of the Synopsys, Inc., nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
--------------------------------------------- */
/**
 *
 * \file
 * \ingroup	BOARD_EMSK_COMMON_INIT
 * \brief	emsk resource definitions
 */

/**
 * \addtogroup BOARD_EMSK_COMMON_INIT
 * @{
 */
#ifndef _EMSK_H_
#define _EMSK_H_

#include "emsk_hardware.h"
#include "arc_em.h"
#include "arc_builtin.h"


#include "dw_uart_obj.h"
#include "dw_gpio_obj.h"
#include "dw_spi_obj.h"

//#include "printf.h"
//#include "dev_pinmux.h"

//#include "common/emsk_timer.h"
//#include "common/emsk_gpio.h"
#include "rtthread.h"
#define PERIPHERAL_BASE		 (0xB0000000)

/* common macros must be defined by all boards */

#define BOARD_CONSOLE_UART_ID		DW_UART_0_ID
#define BOARD_CONSOLE_UART_BAUD		UART_BAUDRATE_115200
//#define BOARD_ADC_IIC_ID			DW_IIC_0_ID
//#define BOARD_TEMP_SENSOR_IIC_ID	DW_IIC_0_ID
//#define BOARD_TEMP_IIC_SLVADDR	TEMP_I2C_SLAVE_ADDRESS
//#define BOARD_SDCARD_SPI_ID		DW_SPI_0_ID

#define BOARD_SFLASH_SPI_ID			DW_SPI_3_ID
#define BOARD_SFLASH_SPI_LIN		EMSK_SPI_LINE_SFLASH

/** board doesn`t exist flash device */
#define BOARD_FLASH_EXIST			(1)

#ifndef BOARD_SPI_FREQ
#define BOARD_SPI_FREQ				(500000)
#endif


#define BOARD_SYS_TIMER_ID			TIMER_0
#define BOARD_SYS_TIMER_INTNO		INTNO_TIMER0
#define BOARD_SYS_TIMER_HZ			(1000)

/** board timer count frequency (HZ) */
#define BOARD_SYS_TIMER_MS_HZ		(1000)
/** board timer count frequency convention based on the global timer counter */
#define BOARD_SYS_TIMER_MS_CONV		(BOARD_SYS_TIMER_MS_HZ/BOARD_SYS_TIMER_HZ)

#define BOARD_OS_TIMER_ID			TIMER_0
#define BOARD_OS_TIMER_INTNO		INTNO_TIMER0

#define BOARD_CPU_CLOCK				CLK_CPU
#define BOARD_DEV_CLOCK				CLK_BUS_APB

#define BOARD_LED_MASK			(0x1ff)
#define BOARD_LED_CNT			(9)
#define BOARD_BTN_MASK			(0x7)
#define BOARD_BTN_CNT			(3)
#define BOARD_SWT_MASK			(0xf)
#define BOARD_SWT_CNT			(4)

#define BOARD_ONBOARD_NTSHELL_ID	(EMSK_NTSHELL_0_ID)
#define NTSHELL_CONSOLE_ID		(EMSK_NTSHELL_0_ID)
#define NTSHELL_NETWORK_ID		(EMSK_NTSHELL_1_ID)

#define OSP_DELAY_OS_COMPAT_ENABLE	(1)
#define OSP_DELAY_OS_COMPAT_DISABLE	(0)

#define OSP_GET_CUR_SYSHZ()		(gl_emsk_sys_hz_cnt)
#define OSP_GET_CUR_MS()		(gl_emsk_ms_cnt)
#define OSP_GET_CUR_US()		board_get_cur_us()
#define OSP_GET_CUR_HWTICKS()		board_get_hwticks()

/*          				chip_mode   mode_sel[1:0]
 * BOOT_SPI_FLAHS_MODe     0  00
 * BOOT_CAN_MODE		   0  01
 * BOOT_UART_MODE          0  10
 * PROGRAM_CAN_MODE        0  11
 * PROGRAM_UART_MODE       1  00
 * PROGRAM_EFUSE_MODE      1  01
 * MBIST_MODE              1  10
 */
typedef enum{
	BOOT_SPI_FLASH_MODE=0,
	BOOT_CAN_MODE,
	BOOT_UART_MODE,
	PROGRAM_CAN_MODE,
	PROGRAM_UART_MODE,
	PROGRAM_EFUSE_MODE,
	MBIST_MODE,
}chip_mode_t;

struct board_cfg{
	chip_mode_t chip_mode;
};

extern struct board_cfg g_board_cfg;

#ifdef __cplusplus
extern "C" {
#endif


extern void board_init(void);
extern void board_timer_update(uint32_t precision);
//extern void board_delay_ms(uint32_t ms, uint8_t os_compat);
extern uint64_t board_get_hwticks(void);
extern uint64_t board_get_cur_us(void);

extern chip_mode_t get_chip_mode();
extern uint32_t get_chipid();

#ifdef __cplusplus
}
#endif

#define dbg(fmt, args...)  rt_kprintf(fmt, ##args)
#endif	/* _EMSK_H_ */

/** @} end of group BOARD_EMSK_COMMON_INIT */
