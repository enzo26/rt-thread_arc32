/* ------------------------------------------
 * Copyright (c) 2017, Synopsys, Inc. All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:

 * 1) Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * 2) Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.

 * 3) Neither the name of the Synopsys, Inc., nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
--------------------------------------------- */
/**
 *
 * \file
 * \ingroup	BOARD_EMSK_COMMON_INIT
 * \brief	emsk hardware resource definitions
 */

/**
 * \addtogroup BOARD_EMSK_COMMON_INIT
 * @{
 */
#ifndef _EMSK_HARDWARE_H_
#define _EMSK_HARDWARE_H_

#include "arc_feature_config.h"


/** CPU Clock Frequency definition */
#if defined(BOARD_CPU_FREQ)
	/*!< Get cpu clock frequency definition from build system */
	#define CLK_CPU			(BOARD_CPU_FREQ)
#elif defined(ARC_FEATURE_CPU_CLOCK_FREQ)
#if defined(ASIC_BOARD) || defined(RTL_SIM_BOARD)
	/*!< Get cpu clock frequency definition from tcf file */
	#define CLK_CPU			(ARC_FEATURE_CPU_CLOCK_FREQ)
#elif defined(FPGA_BOARD)
//#define CLK_CPU				(200000000/8U)   //20200421 bit
#define CLK_CPU				(200000000/4U)   //20200428 bit

#endif
#else
	/*!< Default cpu clock frequency */
	#define CLK_CPU			(20000000)
#endif


/** Peripheral Bus Reference Clock definition */
#if defined(ASIC_BOARD)
	/*!< Default peripheral bus reference clock defintion */
	#define CLK_BUS_APB		(160000000U)
#elif defined(FPGA_BOARD)
	//#define CLK_BUS_APB		(160000000U/8U)   //20200421 bit
	#define CLK_BUS_APB		(160000000U/4U)  //20200428 bit
#endif

/* Device Register Base Address */
//#define REL_REGBASE_PINMUX		(0x00000000U)		/*!< PINMUX */
//#define REL_REGBASE_SPI_MST_CS_CTRL	(0x00000014U)		/*!< SPI Master Select Ctrl */
#define REL_REGBASE_GPIO0		(0x02018000U)		/*!< GPIO 0 Onboard */
#define REL_REGBASE_TIMERS		(0x0200D000U)		/*!< DW TIMER */
//#define REL_REGBASE_I2C0		(0x00004000U)		/*!< I2C 0 */
//#define REL_REGBASE_I2C1		(0x00005000U)		/*!< I2C 1 */
#define REL_REGBASE_SPI0		(0x02009000U)		/*!< SPI A  */
#define REL_REGBASE_SPI1		(0x0200A000U)		/*!< SPI B   */
#define REL_REGBASE_SPI2		(0x02016000U)		/*!< SPI C   */

#define QSPI_FLASH_ABS_BASE			(0xb1004000U)

#define REL_REGBASE_UART0		(0x02007000U)		/*!< UART0  */
#define REL_REGBASE_UART1		(0x02008000U)		/*!< UART1  */
#define REL_REGBASE_UART2		(0x02013000U)		/*!< UART2 */
//#define REL_REGBASE_WDT			(0x0000B000U)		/*!< WDT  */
// #define REL_REGBASE_I2S_MASTER_IN	(0x0000C000U)		/*!< I2S Master In  */
// #define REL_REGBASE_I2S_MASTER_OUT	(0x0000D000U)		/*!< I2S Master Out  */
// #define REL_REGBASE_GMAC		(0x0000E000U)		/*!< GMAC  */

/* Interrupt Connection */
//#define INTNO_DMA_START			22			/*!< Core DMA Controller */
//#define INTNO_DMA_COMPLETE		22			/*!< Core DMA Controller Complete */
//#define INTNO_DMA_ERROR			23			/*!< Core DMA Controller Error */
#define INTNO_GPIO					33			/*!< GPIO controller */
//#define INTNO_I2C0				25			/*!< I2C_0 controller */
//#define INTNO_I2C1				26			/*!< I2C_1 controller */

#define INTNO_UART0			22			/*!< UART0 */
#define INTNO_UART1			23			/*!< UART1 */
#define INTNO_UART2			24			/*!< UART2 */
#define INTNO_SPI_0			25			/*!< SPI 0 A> */
#define INTNO_SPI_1			26			/*!< SPI 1 B> */
#define INTNO_SPI_2			27			/*!< SPI 2 C> */
#define INTNO_SPI_3    		30          /*!< qspi flash> */

//DCAN interrupt
#define INTNO_DCAN_RX_WARNING				(34)
#define INTNO_DCAN_TX_WARNING				(35)
#define INTNO_DCAN_WAKEIN					(36)
#define INTNO_DCAN_ERROR					(37)
#define INTNO_DCAN_BUSOFF					(38)
#define INTNO_DCAN_WAKEMATCH_AND_TIMEOUT	(39)
#define INTNO_DCAN_OTHERS					(40)
#define INTNO_DCAN_MB_31_0					(41)
#define INTNO_DCAN_MB_63_32					(42)
#define INTNO_DCAN_MB_95_64					(43)
#define INTNO_DCAN_MB_127_96				(44)
#define INTNO_DCAN_NECHA					(45)
#define INTNO_DCAN_NECFA					(46)


#define INTNO_CANFD_RX_WARNING				(47)
#define INTNO_CANFD_TX_WARNING				(48)
#define INTNO_CANFD_WAKEIN					(49)
#define INTNO_CANFD_ERROR					(50)
#define INTNO_CANFD_BUSOFF					(51)
#define INTNO_CANFD_WAKEMATCH_AND_TIMEOUT	(52)
#define INTNO_CANFD_OTHERS					(53)
#define INTNO_CANFD_MB_31_0					(54)
#define INTNO_CANFD_MB_63_32				(55)
#define INTNO_CANFD_MB_95_64				(56)
#define INTNO_CANFD_MB_127_96				(57)
#define INTNO_CANFD_NECHA					(58)
#define INTNO_CANFD_NECFA					(59)

//#define INTNO_DW_WDT			32			/*!< WDT */
#define INTNO_DW_TMR0			63			/*!< DW Timer 0 */
#define INTNO_DW_TMR1			64			/*!< DW Timer 1 */
// #define INTNO_I2S_Master_In		33			/*!< I2S Master In */
// #define INTNO_I2S_Master_Out		34			/*!< I2S Master Out */


/* SPI Mater Signals Usage */
#define	 EMSK_SPI_LINE_SFLASH			0			/*!< CS5 -- On-board SPI Flash memory */


#define SOC_REG_ADDR  (0xb2011000)
#define SOC_CFG_REG00_OFFSET   (0)
#define SOC_CFG_REG01_OFFSET   (4)
#define SOC_CFG_REG04_OFFSET   (16)
#define SOC_CFG_REG0B_OFFSET   (0x2C)
#define SOC_MODE_REG_OFFSET	(0xff0)

#define CHIPID_SHIFT		(7)
#define CHIPID_MASK			(0x7<<CHIPID_SHIFT)
#define CHIP_MODE_SHIFT		(0)
#define CHIP_MODE_MASK		(0x7F<<CHIP_MODE_SHIFT)

#define SOC_MODE_REG	*(volatile uint32_t *)(SOC_REG_ADDR + SOC_MODE_REG_OFFSET)



#endif	/* _EMSK_HARDWARE_H_ */

/** @} end of group BOARD_EMSK_COMMON_INIT */
