
#include "can_hw_access.h"

#undef dbg
#define dbg
/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ExitFreezeMode
 * Description   : Exit of freeze mode.
 *
 *END**************************************************************************/
void FLEXCAN_ExitFreezeMode(CAN_Type * base) {
	base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
	base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);

	/* Wait till exit freeze mode */
	while (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) != 0U) {
	}
	dbg("%s: base=0x%x \n", __FUNCTION__, base);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_EnterFreezeMode
 * Description   : Enter the freeze mode.
 *
 *END**************************************************************************/
void FLEXCAN_EnterFreezeMode(CAN_Type * base) {
	bool enabled = false;

	base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
	base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);
	if (((base->MCR & CAN_MCR_MDIS_MASK) >> CAN_MCR_MDIS_SHIFT) == 0U) {
		enabled = true;
	} else {
		base->MCR &= ~CAN_MCR_MDIS_MASK;
	}
//	dbg("0x%x:MCR: 0x%x\n", base, base->MCR);
#if defined(ERRATA_E9595)
	/* Check Low-Power Mode Acknowledge Cleared */
	while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U) {}
	/* Check if is a Bus-Off Error corresponding to 1x */
	if ((((base->ESR1 & CAN_ESR1_FLTCONF_MASK) >> CAN_ESR1_FLTCONF_SHIFT) & 2U) != 0U)
	{
		/* Save registers before Soft Reset */
		uint32_t tempIMSK[2],tempMCR;
		tempIMSK[0] = base->IMASK1;
		tempIMSK[1] = base->IMASK2;
		tempMCR = base->MCR;
		/* Soft Reset FlexCan */
		base->MCR |= CAN_MCR_SOFTRST(1U);
		while (((base->MCR & CAN_MCR_SOFTRST_MASK) >> CAN_MCR_SOFTRST_SHIFT) != 0U) {}
		/* Restore registers after Soft Reset */
		base->IMASK1 = tempIMSK[0];
		base->IMASK2 = tempIMSK[1];
		base->MCR = tempMCR;
#if FEATURE_CAN_HAS_MEM_ERR_DET
		/* Disable the Protection again because is enabled by soft reset */
		FLEXCAN_DisableMemErrorDetection(base);
#endif
	}
	else
	{
		base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);
	}
#endif
#if defined(ERRATA_E8341)
	base->TIMER = 0U;
	uint32_t aux=0U;
	while ((((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) == 0U) && (aux < 730U))
	{
		/* Wait until finish counting 730 bit times and exit*/
		aux = (uint32_t)base->TIMER;
	}
	if (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) == 0U)
	{
		/* Save registers before Soft Reset */
		uint32_t tempIMSK[2],tempMCR;
		tempIMSK[0] = base->IMASK1;
		tempIMSK[1] = base->IMASK2;
		tempMCR = base->MCR;
		/* Soft Reset FlexCan */
		base->MCR |= CAN_MCR_SOFTRST(1U);
		while (((base->MCR & CAN_MCR_SOFTRST_MASK) >> CAN_MCR_SOFTRST_SHIFT) != 0U) {}
		/* Restore registers after Soft Reset */
		base->IMASK1 = tempIMSK[0];
		base->IMASK2 = tempIMSK[1];
		base->MCR = tempMCR;
#if FEATURE_CAN_HAS_MEM_ERR_DET
		/* Disable the Protection again because is enabled by soft reset */
		FLEXCAN_DisableMemErrorDetection(base);
#endif
	}
#endif

	/* Wait for entering the freeze mode */
	while (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) == 0U) {
	}
	if (false == enabled) {
		base->MCR |= CAN_MCR_MDIS_MASK;
		/* Wait until disable mode acknowledged */
		while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 0U) {
		}
	}
	dbg("Freeze mode\n");
}


/*FUNCTION**********************************************************************
 *
 * Function Name: FLEXCAN_ComputeDLCValue
 * Description  : Computes the DLC field value, given a payload size (in bytes).
 *
 *END**************************************************************************/
static uint8_t FLEXCAN_ComputeDLCValue(
        uint8_t payloadSize)
{
    uint32_t ret = 0xFFU;     				/* 0,  1,  2,  3,  4,  5,  6,  7,  8, */
    static const uint8_t payload_code[65] = { 0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U,
    						/* 9 to 12 payload have DLC Code 12 Bytes */
    					CAN_DLC_VALUE_12_BYTES, CAN_DLC_VALUE_12_BYTES, CAN_DLC_VALUE_12_BYTES, CAN_DLC_VALUE_12_BYTES,
							/* 13 to 16 payload have DLC Code 16 Bytes */
						CAN_DLC_VALUE_16_BYTES, CAN_DLC_VALUE_16_BYTES, CAN_DLC_VALUE_16_BYTES, CAN_DLC_VALUE_16_BYTES,
    						/* 17 to 20 payload have DLC Code 20 Bytes */
						CAN_DLC_VALUE_20_BYTES, CAN_DLC_VALUE_20_BYTES, CAN_DLC_VALUE_20_BYTES, CAN_DLC_VALUE_20_BYTES,
							/* 21 to 24 payload have DLC Code 24 Bytes */
						CAN_DLC_VALUE_24_BYTES, CAN_DLC_VALUE_24_BYTES, CAN_DLC_VALUE_24_BYTES, CAN_DLC_VALUE_24_BYTES,
							/* 25 to 32 payload have DLC Code 32 Bytes */
						CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES,
						CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES, CAN_DLC_VALUE_32_BYTES,
							/* 33 to 48 payload have DLC Code 48 Bytes */
						CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES,
						CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES,
						CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES,
						CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES, CAN_DLC_VALUE_48_BYTES,
							/* 49 to 64 payload have DLC Code 64 Bytes */
						CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES,
						CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES,
						CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES,
						CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES, CAN_DLC_VALUE_64_BYTES };

    if (payloadSize <= 64U)
    {
    	ret = payload_code[payloadSize];
    }
    else
    {
        /* The argument is not a valid payload size will return 0xFF*/
    }

    return (uint8_t)ret;
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ComputePayloadSize
 * Description   : Computes the maximum payload size (in bytes), given a DLC
 * field value.
 *
 *END**************************************************************************/
static uint8_t FLEXCAN_ComputePayloadSize(uint8_t dlcValue)
{
    uint8_t ret=0U;

    if (dlcValue <= 8U)
    {
        ret = dlcValue;
    }
#if FEATURE_CAN_HAS_FD
    else
    {
        switch (dlcValue) {
        case CAN_DLC_VALUE_12_BYTES:
            ret = 12U;
            break;
        case CAN_DLC_VALUE_16_BYTES:
            ret = 16U;
            break;
        case CAN_DLC_VALUE_20_BYTES:
            ret = 20U;
            break;
        case CAN_DLC_VALUE_24_BYTES:
            ret = 24U;
            break;
        case CAN_DLC_VALUE_32_BYTES:
            ret = 32U;
            break;
        case CAN_DLC_VALUE_48_BYTES:
            ret = 48U;
            break;
        case CAN_DLC_VALUE_64_BYTES:
            ret = 64U;
            break;
        default:
            /* The argument is not a valid DLC size */
            break;
        }
    }
#endif /* FEATURE_CAN_HAS_FD */
    return ret;
}



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ClearRAM
 * Description   : Clears FlexCAN memory positions that require initialization.
 *
 *END**************************************************************************/
static void FLEXCAN_ClearRAM(CAN_Type * base)
{
    uint32_t databyte;
    uint32_t RAM_size = FLEXCAN_GetMaxMbNum(base) * 4U;
    uint32_t RXIMR_size = FLEXCAN_GetMaxMbNum(base);
    __IO uint32_t  *RAM = base->RAMn;

    /* Clear MB region */
    for (databyte = 0; databyte < RAM_size; databyte++) {
        RAM[databyte] = 0x0;
    }

    RAM = base->RXIMR;

    /* Clear RXIMR region */
    for (databyte = 0; databyte < RXIMR_size; databyte++) {
        RAM[databyte] = 0x0;
    }

#if FEATURE_CAN_HAS_MEM_ERR_DET
    /* Set WRMFRZ bit in CTRL2 Register to grant write access to memory */
    base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK) | CAN_CTRL2_WRMFRZ(1U);

    uint32_t ram_addr = (uint32_t)base + (uint32_t)FEATURE_CAN_RAM_OFFSET;
    RAM = (volatile uint32_t *)ram_addr;

#if FEATURE_CAN_ECC_CLEAR_RXMASKS
    /* Clear RXMGMASK, RXFGMASK, RX14MASK, RX15MASK RAM mapping */
    base->RXMGMASK = 0U;
    base->RXFGMASK = 0U;
    base->RX14MASK = 0U;
    base->RX15MASK = 0U;
#endif

#if FEATURE_CAN_HAS_FD
    /* Clear SMB FD region */
    for (databyte = FEATURE_CAN_SMB_FD_START_ADDRESS_OFFSET; databyte < (uint32_t)FEATURE_CAN_SMB_FD_END_ADDRESS_OFFSET; databyte++) {
            RAM[databyte] = 0;
    }
#endif

    /* Clear WRMFRZ bit in CTRL2 Register to restrict write access to memory */
    base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK) | CAN_CTRL2_WRMFRZ(0U);
#endif
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_GetMsgBuffRegion
 * Description   : Returns the start of a MB area, based on its index.
 *
 *END**************************************************************************/
__IO uint32_t * FLEXCAN_GetMsgBuffRegion(
        CAN_Type * base,
        uint32_t msgBuffIdx)
{
#if FEATURE_CAN_HAS_FD
    uint8_t payload_size = FLEXCAN_GetPayloadSize(base);
#else
    uint8_t payload_size = 8U;
#endif

    uint8_t arbitration_field_size = 8U;
    uint32_t ramBlockSize = 512U;
    uint32_t ramBlockOffset;

    uint8_t mb_size = (uint8_t)(payload_size + arbitration_field_size);
    uint8_t maxMbNum = (uint8_t)(ramBlockSize / mb_size);

    ramBlockOffset = 128U * (msgBuffIdx / (uint32_t)maxMbNum);

    /* Multiply the MB index by the MB size (in words) */
    uint32_t mb_index = ramBlockOffset + ((msgBuffIdx % (uint32_t)maxMbNum) * ((uint32_t)mb_size >> 2U));

    return &(base->RAMn[mb_index]);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_Enable
 * Description   : Enable FlexCAN module.
 * This function will enable FlexCAN module.
 *
 *END**************************************************************************/
void FLEXCAN_Enable(CAN_Type * base) {
	dbg("%s:0x%x, MCR=0x%x\n", __FUNCTION__, base, base->MCR);
	/* Check for low power mode */
	if (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U) {
		/* Enable clock */
		base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(0U);
		base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);
		base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);

		/* Wait until enabled */
		while (((base->MCR & CAN_MCR_NOTRDY_MASK) >> CAN_MCR_NOTRDY_SHIFT) != 0U) {

		}
	}

	dbg("%s end, MCR:0x%x\n", __FUNCTION__, base->MCR);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_Disable
 * Description   : Disable FlexCAN module.
 * This function will disable FlexCAN module.
 *
 *END**************************************************************************/
void FLEXCAN_Disable(CAN_Type * base) {
	/* To access the memory mapped registers */
	/* Entre disable mode (hard reset). */
	if (((base->MCR & CAN_MCR_MDIS_MASK) >> CAN_MCR_MDIS_SHIFT) == 0U) {
		/* Clock disable (module) */
		base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(1U);

		/* Wait until disable mode acknowledged */
		while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 0U) {
		}
	}
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_SetOperationMode
 * Description   : Enable a FlexCAN operation mode.
 * This function will enable one of the modes listed in flexcan_operation_modes_t.
 *
 *END**************************************************************************/
void FLEXCAN_SetOperationMode(CAN_Type * base, flexcan_operation_modes_t mode)
{

	switch (mode)
	{
	case FLEXCAN_FREEZE_MODE:
        /* Debug mode, Halt and Freeze*/
        FLEXCAN_EnterFreezeMode(base);
	break;
	case FLEXCAN_DISABLE_MODE:
		/* Debug mode, Halt and Freeze */
		base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(1U);
	break;
	case FLEXCAN_NORMAL_MODE:
#if FEATURE_CAN_HAS_SUPV
        base->MCR = (base->MCR & ~CAN_MCR_SUPV_MASK) | CAN_MCR_SUPV(0U);
#endif
        base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LOM_MASK) | CAN_CTRL1_LOM(0U);
        base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LPB_MASK) | CAN_CTRL1_LPB(0U);
    break;
	case FLEXCAN_LISTEN_ONLY_MODE:
        base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LOM_MASK) | CAN_CTRL1_LOM(1U);
    break;
	case FLEXCAN_LOOPBACK_MODE:
        base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LPB_MASK) | CAN_CTRL1_LPB(1U);
        base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LOM_MASK) | CAN_CTRL1_LOM(0U);
        /* Enable Self Reception */
        FLEXCAN_SetSelfReception(base, true);
    break;
	default :
		 /* Should not get here */
	break;
	}
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_Init
 * Description   : Initialize FlexCAN module.
 * This function will reset FlexCAN module, set maximum number of message
 * buffers, initialize all message buffers as inactive, enable RX FIFO
 * if needed, mask all mask bits, and disable all MB interrupts.
 *
 *END**************************************************************************/
void FLEXCAN_Init(CAN_Type * base)
{
	dbg("%s\n", __FUNCTION__);
#if 1
    /* Reset the FLEXCAN */
    base->MCR = (base->MCR & ~CAN_MCR_SOFTRST_MASK) | CAN_MCR_SOFTRST(1U);

    /* Wait for reset cycle to complete */
    while (((base->MCR & CAN_MCR_SOFTRST_MASK) >> CAN_MCR_SOFTRST_SHIFT) != 0U) {
    }
#endif
    /* Enable abort */
#ifndef ERRATA_E9527
    /* Avoid Abort Transmission, use Inactive MB */
    //base->MCR = (base->MCR & ~CAN_MCR_AEN_MASK) | CAN_MCR_AEN(1U);
#endif

    /* Clear FlexCAN memory */
    FLEXCAN_ClearRAM(base);
    if(base == g_can[1]){ //FD interface need init smb buffer region
		base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK ) | CAN_CTRL2_WRMFRZ(1);
	//?? init smb region for 0xF28~0xFFF
		uint8_t *can_base = (uint8_t *)base;
		dbg("can_base: 0x%x\n", can_base);

		for(uint32_t i =FEATURE_CAN_SMB_FD_START_ADDR; i< FEATURE_CAN_SMB_FD_END_ADDR; i++){
			can_base[i] = 0;
		}
		base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK ) | CAN_CTRL2_WRMFRZ(0);
    }
    /* Rx global mask*/
    (base->RXMGMASK) = (uint32_t)(CAN_RXMGMASK_MG_MASK);

    /* Rx reg 14 mask*/
    (base->RX14MASK) =  (uint32_t)(CAN_RX14MASK_RX14M_MASK);

    /* Rx reg 15 mask*/
    (base->RX15MASK) = (uint32_t)(CAN_RX15MASK_RX15M_MASK);

    /* Disable all MB interrupts */
    (base->IMASK1) = 0x0;
    /* Clear all MB interrupt flags */
    (base->IFLAG1) = CAN_IMASK1_BUF31TO0M_MASK;



    if (FLEXCAN_GetMaxMbNum(base) > 32U)
    {
		(base->IMASK2) = 0x0;
		(base->IFLAG2) = CAN_IMASK2_BUF63TO32M_MASK;
	}


    if (FLEXCAN_GetMaxMbNum(base) > 64U)
    {
    	(base->IMASK3) = 0x0;
    	(base->IFLAG3) = CAN_IMASK3_BUF95TO64M_MASK;
    }


    if (FLEXCAN_GetMaxMbNum(base) > 96U)
       {
       	(base->IMASK4) = 0x0;
       	(base->IFLAG4) = CAN_IMASK4_BUF127TO96M_MASK;
       }



    /* Clear all error interrupt flags */
    (base->ESR1) = FLEXCAN_ALL_INT;
}



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_GetMsgBuff
 * Description   : Get a message buffer field values.
 * This function will first check if RX FIFO is enabled. If RX FIFO is enabled,
 * the function will make sure if the MB requested is not occupied by RX FIFO
 * and ID filter table. Then this function will get the message buffer field
 * values and copy the MB data field into user's buffer.
 *
 *END**************************************************************************/
void FLEXCAN_GetMsgBuff(
    CAN_Type * base,
    uint32_t msgBuffIdx,
    can_msgbuff_t *msgBuff)
{
    DEV_ASSERT(msgBuff != NULL);

    uint8_t i;

    __IO  uint32_t *flexcan_mb = FLEXCAN_GetMsgBuffRegion(base, msgBuffIdx);
    __IO  uint32_t *flexcan_mb_id   = &flexcan_mb[1];
    volatile  uint8_t  *flexcan_mb_data = (volatile uint8_t *)(&flexcan_mb[2]);
    __IO  uint32_t *flexcan_mb_data_32 = &flexcan_mb[2];
    uint32_t *msgBuff_data_32 = (uint32_t *)(msgBuff->data);
    uint32_t mbWord;

    uint8_t flexcan_mb_dlc_value = (uint8_t)(((*flexcan_mb) & CAN_CS_DLC_MASK) >> 16);
    uint8_t payload_size = FLEXCAN_ComputePayloadSize(flexcan_mb_dlc_value);
    dbg("recv dlc=%d, payload_size=%d\r\n", flexcan_mb_dlc_value, payload_size);
//    dbg("recv addr=%x\n", flexcan_mb);
#if FEATURE_CAN_HAS_FD
    if (payload_size > FLEXCAN_GetPayloadSize(base))
    {
        payload_size = FLEXCAN_GetPayloadSize(base);
    }
#endif /* FEATURE_CAN_HAS_FD */

	msgBuff->data_len = payload_size;

    /* Get a MB field values */
    msgBuff->cs = *flexcan_mb;
    if ((msgBuff->cs & CAN_CS_IDE_MASK) != 0U)
    {
        msgBuff->msg_id = (*flexcan_mb_id);
    }
    else
    {
        msgBuff->msg_id = (*flexcan_mb_id) >> CAN_ID_STD_SHIFT;
    }

    for (i = 0U ; i < (payload_size & ~3U); i += 4U)
    {
        mbWord = flexcan_mb_data_32[i >> 2U];
        FlexcanSwapBytesInWord(mbWord, msgBuff_data_32[i >> 2U]);
    }

    for ( ; i < payload_size ; i++)
    {	/* Max allowed value for index is 63 */
        msgBuff->data[i] = flexcan_mb_data[FlexcanSwapBytesInWordIndex(i)];
    }

}

/*!
 * @brief Clears the interrupt flag of the message buffers.
 *
 * @param   base  		The FlexCAN base address
 * @param   msgBuffIdx  Index of the message buffer
 */
void FLEXCAN_ClearMsgBuffIntStatusFlag(CAN_Type * base, uint32_t msgBuffIdx)
{
    uint32_t flag = ((uint32_t)1U << (msgBuffIdx % 32U));

    /* Clear the corresponding message buffer interrupt flag*/
    if (msgBuffIdx < 32U)
    {
        (base->IFLAG1) = (flag);
    }
    else if (msgBuffIdx < 64U)
    {
        (base->IFLAG2) = (flag);
    }
    else if(msgBuffIdx < 96U)
    {
    	(base->IFLAG3) = (flag);
    }else{
    	(base->IFLAG4) = (flag);
    }

}
/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ConfigPN
 * Description   : Configures the Pretended Networking mode.
 *
 *END**************************************************************************/
void FLEXCAN_ConfigPN(CAN_Type * base, const flexcan_pn_config_t *pnConfig)
{
    assert(pnConfig != NULL);

    /* Configure specific pretended networking settings */
    FLEXCAN_SetPNFilteringSelection(base,
                                    pnConfig->wakeUpTimeout,
                                    pnConfig->wakeUpMatch,
                                    pnConfig->numMatches,
                                    pnConfig->filterComb,
                                    pnConfig->idFilterType,
                                    pnConfig->payloadFilterType);

    FLEXCAN_SetPNTimeoutValue(base, pnConfig->matchTimeout);

    /* Configure ID filtering */
    FLEXCAN_SetPNIdFilter1(base, pnConfig->idFilter1);

    /* Configure the second ID, if needed (as mask for exact matching or higher limit for range matching) */
    if ((pnConfig->idFilterType == FLEXCAN_FILTER_MATCH_EXACT) || (pnConfig->idFilterType == FLEXCAN_FILTER_MATCH_RANGE))
    {
        FLEXCAN_SetPNIdFilter2(base, pnConfig);
    }

    /* Configure payload filtering, if requested */
    if ((pnConfig->filterComb == FLEXCAN_FILTER_ID_PAYLOAD) || (pnConfig->filterComb == FLEXCAN_FILTER_ID_PAYLOAD_NTIMES))
    {
        FLEXCAN_SetPNDlcFilter(base,
                               pnConfig->payloadFilter.dlcLow,
                               pnConfig->payloadFilter.dlcHigh);

        FLEXCAN_SetPNPayloadHighFilter1(base, pnConfig->payloadFilter.payload1);
        FLEXCAN_SetPNPayloadLowFilter1(base, pnConfig->payloadFilter.payload1);

        /* Configure the second payload, if needed (as mask for exact matching or higher limit for range matching) */
        if ((pnConfig->payloadFilterType == FLEXCAN_FILTER_MATCH_EXACT) || (pnConfig->payloadFilterType == FLEXCAN_FILTER_MATCH_RANGE))
        {
            FLEXCAN_SetPNPayloadHighFilter2(base, pnConfig->payloadFilter.payload2);
            FLEXCAN_SetPNPayloadLowFilter2(base, pnConfig->payloadFilter.payload2);
        }
    }
}


uint32_t FLEXCAN_GetMaxMbNum(const CAN_Type * base)
{
    return FEATURE_CAN_MAX_MB_NUM;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_SetMsgBuffIntCmd
 * Description   : Enable/Disable the corresponding Message Buffer interrupt.
 *
 *END**************************************************************************/
int32_t FLEXCAN_SetMsgBuffIntCmd(
    CAN_Type * base,
    uint32_t msgBuffIdx, bool enable)
{
    uint32_t temp;
    status_t stat = 0;

    if (msgBuffIdx > (((base->MCR) & CAN_MCR_MAXMB_MASK) >> CAN_MCR_MAXMB_SHIFT))
    {
        stat = -1;
    }

    if (stat == 0)
    {
        /* Enable the corresponding message buffer Interrupt */
        temp = 1UL << (msgBuffIdx % 32U);
        if (msgBuffIdx  < 32U)
        {
            if (enable)
            {
                (base->IMASK1) = ((base ->IMASK1) | (temp));
            }
            else
            {
                (base->IMASK1) = ((base->IMASK1) & ~(temp));
            }
        }

        if ((msgBuffIdx >= 32U) && (msgBuffIdx < 64U))
        {
            if (enable)
            {
                (base->IMASK2) = ((base ->IMASK2) | (temp));
            }
            else
            {
                (base->IMASK2) = ((base->IMASK2) & ~(temp));
            }
        }


        if (msgBuffIdx >= 64U)
        {
            if (enable)
            {
                (base->IMASK3) = ((base ->IMASK3) | (temp));
            }
            else
            {
                (base->IMASK3) = ((base->IMASK3) & ~(temp));
            }
        }
        if(msgBuffIdx >= 96U){
        	if(enable){
        		  (base->IMASK4) = ((base ->IMASK4) | (temp));
        	}else{
        		 (base->IMASK3) = ((base->IMASK3) & ~(temp));
        	}
        }
    }

    return stat;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_SetPayloadSize
 * Description   : Sets the payload size of the MBs.
 *
 *END**************************************************************************/
void FLEXCAN_SetPayloadSize(
    CAN_Type * base,
    flexcan_fd_payload_size_t payloadSize)
{
    uint32_t tmp;

    DEV_ASSERT(FLEXCAN_IsFDEnabled(base) || (payloadSize == FLEXCAN_PAYLOAD_SIZE_8));

    /* If FD is not enabled, only 8 bytes payload is supported */
    if (FLEXCAN_IsFDEnabled(base))
    {
        tmp = base->FDCTRL;
        tmp &= ~(CAN_FDCTRL_MBDSR0_MASK);
        tmp |= ((uint32_t)payloadSize) << CAN_FDCTRL_MBDSR0_SHIFT;
#if FEATURE_CAN_HAS_MBDSR1
        tmp &= ~(CAN_FDCTRL_MBDSR1_MASK);
        tmp |= ((uint32_t)payloadSize) << CAN_FDCTRL_MBDSR1_SHIFT;
#endif
#if FEATURE_CAN_HAS_MBDSR2
        tmp &= ~(CAN_FDCTRL_MBDSR2_MASK);
        tmp |= ((uint32_t)payloadSize) << CAN_FDCTRL_MBDSR2_SHIFT;
#endif

        base->FDCTRL = tmp;
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_GetPayloadSize
 * Description   : Returns the payload size of the MBs (in bytes).
 *
 *END**************************************************************************/
uint8_t FLEXCAN_GetPayloadSize(const CAN_Type * base)
{
    uint32_t payloadSize;

    /* The standard payload size is 8 bytes */
    if (!FLEXCAN_IsFDEnabled(base))
    {
        payloadSize = 8U;
    }
    else
    {
        payloadSize = 1UL << (((base->FDCTRL & CAN_FDCTRL_MBDSR0_MASK) >> CAN_FDCTRL_MBDSR0_SHIFT) + 3U);
    }

    return (uint8_t)payloadSize;
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_SetErrIntCmd
 * Description   : Enable the error interrupts.
 * This function will enable Error interrupt.
 *
 *END**************************************************************************/
void FLEXCAN_SetErrIntCmd(CAN_Type * base, can_int_type_t errType, bool enable)
{
    uint32_t temp = (uint32_t)errType;
    if (enable)
    {
        if((errType == FLEXCAN_INT_RX_WARNING) || (errType == FLEXCAN_INT_TX_WARNING))
        {
           base->MCR = (base->MCR & ~CAN_MCR_WRNEN_MASK) | CAN_MCR_WRNEN(1U);
        }
#if FEATURE_CAN_HAS_FD
        if (errType == FLEXCAN_INT_ERR)
        {
           base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_ERRMSK_FAST_MASK) | CAN_CTRL2_ERRMSK_FAST(1U);
        }
#endif
        (base->CTRL1) = ((base->CTRL1) | (temp));
    }
    else
    {
    	(base->CTRL1) = ((base->CTRL1) & ~(temp));
#if FEATURE_CAN_HAS_FD
                if (errType == FLEXCAN_INT_ERR)
                {
                   base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_ERRMSK_FAST_MASK) | CAN_CTRL2_ERRMSK_FAST(0U);
                }
#endif
        temp = base->CTRL1;
		if (((temp & (uint32_t)FLEXCAN_INT_RX_WARNING) == 0U) && ((temp & (uint32_t)FLEXCAN_INT_TX_WARNING) == 0U))
		{
		   /* If WRNEN disabled then both FLEXCAN_INT_RX_WARNING and FLEXCAN_INT_TX_WARNING will be disabled */
		   base->MCR = (base->MCR & ~CAN_MCR_WRNEN_MASK) | CAN_MCR_WRNEN(0U);
		}
    }
}

void FLEXCAN_ClearErrIntStatusFlag(CAN_Type * base)
{
    if((base->ESR1 & FLEXCAN_ALL_INT) != 0U)
    {
        (base->ESR1) = FLEXCAN_ALL_INT;
#ifdef ERRATA_E9005
        /* Dummy read as a workaround for errata e9005 to ensure the flags are
        cleared before continuing. */
        (void)(base->ESR1);
#endif
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_SetRxMsgBuff
 * Description   : Configure a message buffer for receiving.
 * This function will first check if RX FIFO is enabled. If RX FIFO is enabled,
 * the function will make sure if the MB requested is not occupied by RX FIFO
 * and ID filter table. Then this function will configure the message buffer as
 * required for receiving.
 *
 *END**************************************************************************/
status_t FLEXCAN_SetRxMsgBuff(
    CAN_Type * base,
    uint32_t msgBuffIdx,
    const can_msgbuff_code_status_t *cs,
    uint32_t msgId)
{
    DEV_ASSERT(cs != NULL);

    uint32_t val1, val2 = 1;

    __IO uint32_t *flexcan_mb = FLEXCAN_GetMsgBuffRegion(base, msgBuffIdx);
    __IO uint32_t *flexcan_mb_id = &flexcan_mb[1];
    status_t stat = 0;
    dbg("rx buff %d addr: %x \n", msgBuffIdx, flexcan_mb);
    if (msgBuffIdx > (((base->MCR) & CAN_MCR_MAXMB_MASK) >> CAN_MCR_MAXMB_SHIFT))
    {
        stat = -1;
    }

#if FEATURE_CAN_ENABLE_RX_FIFO == 1
    /* Check if RX FIFO is enabled */
    if (((base->MCR & CAN_MCR_RFEN_MASK) >> CAN_MCR_RFEN_SHIFT) != 0U)
    {
        /* Get the number of RX FIFO Filters*/
        val1 = (((base->CTRL2) & CAN_CTRL2_RFFN_MASK) >> CAN_CTRL2_RFFN_SHIFT);
        /* Get the number if MBs occupied by RX FIFO and ID filter table*/
        /* the Rx FIFO occupies the memory space originally reserved for MB0-5*/
        /* Every number of RFFN means 8 number of RX FIFO filters*/
        /* and every 4 number of RX FIFO filters occupied one MB*/
        val2 = RxFifoOcuppiedLastMsgBuff(val1);

        if (msgBuffIdx <= val2) {
            stat =  STATUS_CAN_BUFF_OUT_OF_RANGE;
        }
    }
#endif
    if (stat == 0)
    {
        /* Clean up the arbitration field area */
        *flexcan_mb = 0;
        *flexcan_mb_id = 0;

        /* Set the ID according the format structure */
        if (cs->msg_id_type == CAN_MSG_ID_EXT)
        {
            /* Set IDE */
            *flexcan_mb |= CAN_CS_IDE_MASK;

            /* Clear SRR bit */
            *flexcan_mb &= ~CAN_CS_SRR_MASK;

            /* ID [28-0] */
            *flexcan_mb_id &= ~(CAN_ID_STD_MASK | CAN_ID_EXT_MASK);
            *flexcan_mb_id |= (msgId & (CAN_ID_STD_MASK | CAN_ID_EXT_MASK));
        }
        if (cs->msg_id_type == CAN_MSG_ID_STD)
        {
            /* Make sure IDE and SRR are not set */
            *flexcan_mb &= ~(CAN_CS_IDE_MASK | CAN_CS_SRR_MASK);

            /* ID[28-18] */
            *flexcan_mb_id &= ~CAN_ID_STD_MASK;
            *flexcan_mb_id |= (msgId << CAN_ID_STD_SHIFT) & CAN_ID_STD_MASK;
        }

        /* Set MB CODE */
        if (cs->code != (uint32_t)CAN_RX_NOT_USED)
        {
        	dbg("rx code: %x\n", cs->code);
             *flexcan_mb &= ~CAN_CS_CODE_MASK;
             *flexcan_mb |= (cs->code << CAN_CS_CODE_SHIFT) & CAN_CS_CODE_MASK;

        }
    }

    return stat;
}


status_t FLEXCAN_SetTxMsgBuff(CAN_Type * base,
                            uint32_t msgBuffIdx,
                            const can_msgbuff_code_status_t *cs,
                            uint32_t msgId,
                            const uint8_t *msgData,
                            const bool isRemote)
{
    DEV_ASSERT(cs != NULL);
//    CAN_Type * base = g_can[instance];
    uint32_t val1, val2 = 1;
    uint32_t flexcan_mb_config = 0;
    uint32_t databyte;
    uint8_t dlc_value;
    status_t stat =  0;

     __IO uint32_t * flexcan_mb = FLEXCAN_GetMsgBuffRegion(base, msgBuffIdx);
#if ENABLE_CAN_DEBUG == 1
     dbg("flexcan_mb[%d] addr:%x\n", msgBuffIdx, flexcan_mb);
#endif
    __IO uint32_t *flexcan_mb_id   = &flexcan_mb[1];
    volatile uint8_t  *flexcan_mb_data = (volatile uint8_t *)(&flexcan_mb[2]);
    __IO uint32_t *flexcan_mb_data_32 = &flexcan_mb[2];
    const uint32_t *msgData_32 = (const uint32_t *)msgData;

    if (msgBuffIdx > (((base->MCR) & CAN_MCR_MAXMB_MASK) >> CAN_MCR_MAXMB_SHIFT) )
    {
        stat = -1;
    }
#if FEATURE_ENABLE_RX_FIFO
    /* Check if RX FIFO is enabled*/
    if (((base->MCR & CAN_MCR_RFEN_MASK) >> CAN_MCR_RFEN_SHIFT) != 0U)
    {
        /* Get the number of RX FIFO Filters*/
        val1 = (((base->CTRL2) & CAN_CTRL2_RFFN_MASK) >> CAN_CTRL2_RFFN_SHIFT);
        /* Get the number if MBs occupied by RX FIFO and ID filter table*/
        /* the Rx FIFO occupies the memory space originally reserved for MB0-5*/
        /* Every number of RFFN means 8 number of RX FIFO filters*/
        /* and every 4 number of RX FIFO filters occupied one MB*/
        val2 = RxFifoOcuppiedLastMsgBuff(val1);

        if (msgBuffIdx <= val2) {
            stat =  STATUS_CAN_BUFF_OUT_OF_RANGE;
        }
    }
#endif

    if (stat == 0)
    {
#if FEATURE_CAN_HAS_FD
        /* Make sure the BRS bit will not be ignored */
        if (FLEXCAN_IsFDEnabled(base) && cs->enable_brs)
        {
            base->FDCTRL = (base->FDCTRL & ~CAN_FDCTRL_FDRATE_MASK) | CAN_FDCTRL_FDRATE(1U);
        }
        /* Check if the Payload Size is smaller than the payload configured */
        DEV_ASSERT((uint8_t)cs->dataLen <= FLEXCAN_GetPayloadSize(base));
#else
        DEV_ASSERT((uint8_t)cs->dataLen <= 8U);
#endif
        /* Compute the value of the DLC field */
        dlc_value = FLEXCAN_ComputeDLCValue((uint8_t)cs->data_len);

        /* Copy user's buffer into the message buffer data area */
        if (msgData != NULL)
        {

            for (databyte = 0; databyte < (cs->data_len & ~3U); databyte += 4U)
			{
				FlexcanSwapBytesInWord(msgData_32[databyte >> 2U], flexcan_mb_data_32[databyte >> 2U]);
			}

            for ( ; databyte < cs->data_len; databyte++)
            {
                flexcan_mb_data[FlexcanSwapBytesInWordIndex(databyte)] =  msgData[databyte];
            }
#if FEATURE_CAN_HAS_FD
            uint8_t payload_size = FLEXCAN_ComputePayloadSize(dlc_value);
            /* Add padding, if needed */
            for (databyte = cs->data_len; databyte < payload_size; databyte++)
            {
                flexcan_mb_data[FlexcanSwapBytesInWordIndex(databyte)] = cs->fd_padding;
            }
#endif /* FEATURE_CAN_HAS_FD */
        }

        /* Clean up the arbitration field area */
        *flexcan_mb = 0;
        *flexcan_mb_id = 0;

        /* Set the ID according the format structure */
        if (cs->msg_id_type == CAN_MSG_ID_EXT)
        {
            /* ID [28-0] */
            *flexcan_mb_id &= ~(CAN_ID_STD_MASK | CAN_ID_EXT_MASK);
            *flexcan_mb_id |= (msgId & (CAN_ID_STD_MASK | CAN_ID_EXT_MASK));

            /* Set IDE */
            flexcan_mb_config |= CAN_CS_IDE_MASK;

            /* Clear SRR bit */
            flexcan_mb_config &= ~CAN_CS_SRR_MASK;
        }
        if(cs->msg_id_type == CAN_MSG_ID_STD)
        {
            /* ID[28-18] */
            *flexcan_mb_id &= ~CAN_ID_STD_MASK;
            *flexcan_mb_id |= (msgId << CAN_ID_STD_SHIFT) & CAN_ID_STD_MASK;

            /* make sure IDE and SRR are not set */
            flexcan_mb_config &= ~(CAN_CS_IDE_MASK | CAN_CS_SRR_MASK);
        }

        /* Set the length of data in bytes */
        flexcan_mb_config &= ~CAN_CS_DLC_MASK;
        flexcan_mb_config |= ((uint32_t)dlc_value << CAN_CS_DLC_SHIFT) & CAN_CS_DLC_MASK;
#if ENABLE_CAN_DEBUG == 1
        dbg("%s: dlc=%d\n", __FUNCTION__, dlc_value);
#endif
        /* Set MB CODE */
        if (cs->code != (uint32_t)CAN_TX_NOT_USED)
        {
            if (cs->code == (uint32_t)CAN_TX_REMOTE)
            {
                /* Set RTR bit */
                flexcan_mb_config |= CAN_CS_RTR_MASK;
            }
            else
            {
                if (isRemote == true)
                {
                    /* Set RTR bit */
                    flexcan_mb_config |= CAN_CS_RTR_MASK;
                }
            }

            /* Reset the code */
            flexcan_mb_config &= ~CAN_CS_CODE_MASK;

            /* Set the code */
            if (cs->fd_enable)
            {
                flexcan_mb_config |= ((cs->code << CAN_CS_CODE_SHIFT) & CAN_CS_CODE_MASK) | CAN_MB_EDL_MASK;
            }
            else
            {
                flexcan_mb_config |= (cs->code << CAN_CS_CODE_SHIFT) & CAN_CS_CODE_MASK;
            }

            if (cs->enable_brs)
            {
                flexcan_mb_config |= CAN_MB_BRS_MASK;
            }

            *flexcan_mb |= flexcan_mb_config;
#if ENABLE_CAN_DEBUG == 1
            dbg("mb_config=0x%x\r\n", *flexcan_mb);
#endif
        }
    }

    return stat;
}





