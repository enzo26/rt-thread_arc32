/*
 * mc_can.c
 *
 *  Created on: 2019幄1�7朄1�7旄1�7
 *      Author: wdxc-39
 */
#include <stdint.h>

#include "can.h"
#include "can_hw_access.h"


CAN_Type *g_can[2] = { CAN_0, CAN_1 };
uint32_t g_canMsgIrqs[][FEATURE_CAN_MB_IRQS_MAX_COUNT] = FEATURE_CAN_MB_IRQS;


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_ConfigMb
 * Description   : Configure a Rx message buffer.
 * This function will first check if RX FIFO is enabled. If RX FIFO is enabled,
 * the function will make sure if the MB requested is not occupied by RX FIFO
 * and ID filter table. Then this function will set up the message buffer fields,
 * configure the message buffer code for Rx message buffer as NOT_USED, enable
 * the Message Buffer interrupt, configure the message buffer code for Rx
 * message buffer as INACTIVE, copy user's buffer into the message buffer data
 * area, and configure the message buffer code for Rx message buffer as EMPTY.
 *
 * Implements    : FLEXCAN_DRV_ConfigRxMb_Activity
 *END**************************************************************************/
status_t FLEXCAN_DRV_ConfigRxMb(
    uint8_t instance,
    uint8_t mb_idx,
    can_data_info_t *rx_info,
    uint32_t msg_id)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    DEV_ASSERT(rx_info != NULL);

    status_t result;
    can_msgbuff_code_status_t cs;
    CAN_Type * base = g_can[instance];

    cs.data_len = rx_info->data_len;
    cs.msg_id_type = rx_info->msg_id_type;
#if FEATURE_CAN_HAS_FD
    cs.fd_enable = rx_info->fd_enable;
#endif
    /* Initialize rx mb*/
    cs.code = (uint32_t)CAN_RX_NOT_USED;
    result = FLEXCAN_SetRxMsgBuff(base, mb_idx, &cs, msg_id);

    if (result == 0)
    {
		/* Initialize receive MB*/
		cs.code = (uint32_t)CAN_RX_INACTIVE;
		result = FLEXCAN_SetRxMsgBuff(base, mb_idx, &cs, msg_id);
    }

    if (result == 0)
    {
    	 /* Set up FlexCAN message buffer fields for receiving data*/
    	 cs.code = (uint32_t)CAN_RX_EMPTY;
    	 result = FLEXCAN_SetRxMsgBuff(base, mb_idx, &cs, msg_id);
    }
    if (result == 0)
    {
        /* Clear the message buffer flag if previous remained triggered*/
    	FLEXCAN_ClearMsgBuffIntStatusFlag(base, mb_idx);
    }

    return result;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_StartRxMessageBufferData
 * Description   : Initiate (start) a receive by beginning the process of
 * receiving data and enabling the interrupt.
 * This is not a public API as it is called from other driver functions.
 *
 *END**************************************************************************/
int32_t FLEXCAN_StartRxMessageBufferData(
                    uint8_t instance,
                    uint8_t mb_idx,
                    can_msgbuff_t *data,
                    bool isBlocking
                    )
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);

    int32_t result = 0;
    CAN_Type * base = g_can[instance];
    //can_state_t * state = g_flexcanStatePtr[instance];

    /* Check if the MB index is in range */
    if(mb_idx >= FLEXCAN_GetMaxMbNum(base))
    {
    	return -1;
    }
#if FEATURE_CAN_ENABLE_RX_FIFO==1
    /* Check if RX FIFO is enabled*/
    if (FLEXCAN_IsRxFifoEnabled(base) == true)
    {
    	uint32_t val1, val2;
        /* Get the number of RX FIFO Filters*/
        val1 = (((base->CTRL2) & CAN_CTRL2_RFFN_MASK) >> CAN_CTRL2_RFFN_SHIFT);
        /* Get the number if MBs occupied by RX FIFO and ID filter table*/
        /* the Rx FIFO occupies the memory space originally reserved for MB0-5*/
        /* Every number of RFFN means 8 number of RX FIFO filters*/
        /* and every 4 number of RX FIFO filters occupied one MB*/
        val2 = RxFifoOcuppiedLastMsgBuff(val1);

        if (mb_idx <= val2)
        {
        	return STATUS_CAN_BUFF_OUT_OF_RANGE;
        }
    }
#endif
#if 0
    /* Start receiving mailbox */
    if(state->mbs[mb_idx].state != MC_CAN_MB_IDLE)
    {
        return STATUS_BUSY;
    }
    state->mbs[mb_idx].state = FLEXCAN_MB_RX_BUSY;
    state->mbs[mb_idx].mb_message = data;
    state->mbs[mb_idx].isBlocking = isBlocking;
#endif
    /* Enable MB interrupt*/
    result = FLEXCAN_SetMsgBuffIntCmd(base, mb_idx, true);
#if 0
    if (result != STATUS_SUCCESS)
    {
        state->mbs[mb_idx].state = MC_CAN_MB_IDLE;
    }
#endif
    return result;
}


int32_t FLEXCAN_DRV_Init( uint8_t instance,
		   can_state_t *state,
		   const flexcan_user_config_t *config) {

	CAN_Type *base = g_can[instance];
	uint32_t i, j;
	//dbg("%s\n", __FUNCTION__);
#if 1
	if (FLEXCAN_IsEnabled(base)) {
		FLEXCAN_EnterFreezeMode(base);
		FLEXCAN_Disable(base);
	}
	FLEXCAN_SelectClock(base, config->pe_clock);
	FLEXCAN_Enable(base);
#endif
	FLEXCAN_EnterFreezeMode(base);

	FLEXCAN_Init(base);

	FLEXCAN_DisableMemErrorDetection(base);

	/* Enable/Disable FD and check FD was set as expected. Setting FD as enabled
	 * might fail if the current CAN instance does not support FD. */
	FLEXCAN_SetFDEnabled(base, config->fd_enable);
	if (FLEXCAN_IsFDEnabled(base) != true) {

	} else {
		/* If the FD feature is enabled, enable the Stuff Bit Count, in order to be
		 * ISO-compliant. */
		FLEXCAN_SetStuffBitCount(base, config->fd_enable);
	}

	if(config->flexcanMode != FLEXCAN_LOOPBACK_MODE){
		FLEXCAN_SetSelfReception(base, false);
		dbg("disable loopback\n");
	}else{
		FLEXCAN_SetSelfReception(base, true);
		dbg("enable loopback\n");
	}

	/* Set payload size. */
	FLEXCAN_SetPayloadSize(base, config->payload);

	if (FLEXCAN_IsFDEnabled(base)) {
		dbg("FD Enable\n");
		FLEXCAN_SetExtendedTimeSegments(base, &config->bitrate);
		FLEXCAN_SetFDTimeSegments(base, &config->bitrate_cbt);
	} else {
		dbg("FD Disable\n");
		FLEXCAN_SetTimeSegments(base, &config->bitrate);
	}

	/* Select mode */
	FLEXCAN_SetOperationMode(g_can[instance], config->flexcanMode);

	if (config->flexcanMode != FLEXCAN_FREEZE_MODE) {
		FLEXCAN_ExitFreezeMode(base);
	}

	//dbg("%s end\n", __FUNCTION__);
	return 0;
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_EnableIRQs
 * Description   : This function enable FLEXCAN instance Interrupts
 *
 *END**************************************************************************/
static inline void FLEXCAN_EnableIRQs(uint8_t instance)
{
	uint8_t i;

//    INT_SYS_EnableIRQ(g_canWakeUpIrq[instance]);
//    INT_SYS_EnableIRQ(g_canErrorIrq[instance]);
//    INT_SYS_EnableIRQ(g_flexcanBusOffIrq[instance]);
    for (i = 0; i < FEATURE_CAN_MB_IRQS_MAX_COUNT ; i++)
    {
            int_enable(g_canMsgIrqs[instance][i]);
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_Receive
 * Description   : This function receives a CAN frame into a configured message
 * buffer. The function returns immediately. If a callback is installed, it will
 * be invoked after the frame was received and read into the specified buffer.
 *
 * Implements    : FLEXCAN_DRV_Receive_Activity
 *END**************************************************************************/
status_t FLEXCAN_DRV_Receive(
    uint8_t instance,
    uint8_t mb_idx,
    can_msgbuff_t *data)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);

    int32_t result;

    result = FLEXCAN_StartRxMessageBufferData(instance, mb_idx, data, false);

    return result;
}

int32_t FLEXCAN_DRV_ReceiveBlocking(
    uint8_t instance,
    uint8_t mb_idx,
    can_msgbuff_t *data,
    uint32_t timeout_ms)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    status_t result;
    CAN_Type * base = g_can[instance];

    result = FLEXCAN_StartRxMessageBufferData(instance, mb_idx, data, true);

    if(result == 0)
    {
        status_t status;
#if 0
        status = OSIF_SemaWait(&state->mbs[mb_idx].mbSema, timeout_ms);

        if (status == STATUS_TIMEOUT)
        {
            /* If the flag is set Successful reception else report TimeOut */
			if(FLEXCAN_GetMsgBuffIntStatusFlag(base,mb_idx) == (uint8_t)0U)
			{
				result = STATUS_TIMEOUT;
			}
			/* Disable message buffer interrupt */
			(void)FLEXCAN_SetMsgBuffIntCmd(base, mb_idx, false);
        }

        /* Consider the MB state has been changed by interrupt as frame received */
        if (state->mbs[mb_idx].state == FLEXCAN_MB_IDLE)
        {
        	return STATUS_SUCCESS;
        }

        state->mbs[mb_idx].state = FLEXCAN_MB_IDLE;
#endif
    }

    return result;
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_ConfigTxMb
 * Description   : Configure a Tx message buffer.
 * This function will first check if RX FIFO is enabled. If RX FIFO is enabled,
 * the function will make sure if the MB requested is not occupied by RX FIFO
 * and ID filter table. Then this function will set up the message buffer fields,
 * configure the message buffer code for Tx buffer as INACTIVE, and enable the
 * Message Buffer interrupt.
 *
 * Implements    : FLEXCAN_DRV_ConfigTxMb_Activity
 *END**************************************************************************/
int32_t FLEXCAN_DRV_ConfigTxMb(
    uint8_t instance,
    uint8_t mb_idx,
    can_data_info_t *tx_info,
    uint32_t msg_id)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    DEV_ASSERT(tx_info != NULL);

    can_msgbuff_code_status_t cs;
    CAN_Type * base = g_can[instance];

    /* Initialize transmit mb*/
    cs.data_len = tx_info->data_len;
    cs.msg_id_type = tx_info->msg_id_type;
#if FEATURE_CAN_HAS_FD
    cs.enable_brs = tx_info->enable_brs;
    cs.fd_enable = tx_info->fd_enable;
    cs.fd_padding = tx_info->fd_padding;
#endif

    cs.code = (uint32_t)CAN_TX_INACTIVE;

    return FLEXCAN_SetTxMsgBuff(base, mb_idx, &cs, msg_id, NULL, false);
}
/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_StartSendData
 * Description   : Initiate (start) a transmit by beginning the process of
 * sending data.
 * This is not a public API as it is called from other driver functions.
 *
 *END**************************************************************************/
static status_t FLEXCAN_StartSendData(
                    uint8_t instance,
                    uint8_t mb_idx,
                    can_data_info_t *tx_info,
                    uint32_t msg_id,
                    const uint8_t *mb_data,
                    bool isBlocking
                    )
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    DEV_ASSERT(tx_info != NULL);

    status_t result;
    can_msgbuff_code_status_t cs;
//    flexcan_state_t * state = g_flexcanStatePtr[instance];
    CAN_Type * base = g_can[instance];

    if(mb_idx >= FLEXCAN_GetMaxMbNum(base))
    {
    	return -1;
    }
#if 0
    if (state->mbs[mb_idx].state != FLEXCAN_MB_IDLE)
    {
        return -1;
    }
#endif

    /* Clear message buffer flag */
	FLEXCAN_ClearMsgBuffIntStatusFlag(base, mb_idx);

#if 0
    state->mbs[mb_idx].state = MC_CAN_MB_TX_BUSY;
    state->mbs[mb_idx].isBlocking = isBlocking;
    state->mbs[mb_idx].isRemote = tx_info->is_remote;
#endif

    cs.data_len = tx_info->data_len;
    cs.msg_id_type = tx_info->msg_id_type;

#if FEATURE_CAN_HAS_FD
    cs.fd_enable = tx_info->fd_enable;
    cs.fd_padding = tx_info->fd_padding;
    cs.enable_brs = tx_info->enable_brs;
#endif

    if (tx_info->is_remote)
    {
        cs.code = (uint32_t)CAN_TX_REMOTE;
    }
    else
    {
        cs.code = (uint32_t)CAN_TX_DATA;
    }
    result = FLEXCAN_SetTxMsgBuff(base, mb_idx, &cs, msg_id, mb_data, false);
#if 0
    if (result != STATUS_SUCCESS)
    {
        state->mbs[mb_idx].state = FLEXCAN_MB_IDLE;
    }
#endif
    return result;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_Send
 * Description   : This function sends a CAN frame using a configured message
 * buffer. The function returns immediately. If a callback is installed, it will
 * be invoked after the frame was sent.
 *
 * Implements    : FLEXCAN_DRV_Send_Activity
 *END**************************************************************************/
status_t FLEXCAN_DRV_Send(
    uint8_t instance,
    uint8_t mb_idx,
    can_data_info_t *tx_info,
    uint32_t msg_id,
    const uint8_t *mb_data)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    DEV_ASSERT(tx_info != NULL);

    status_t result;
    CAN_Type * base = g_can[instance];

    result = FLEXCAN_StartSendData(instance, mb_idx, tx_info, msg_id, mb_data, false);
#if 0
    if(result == STATUS_SUCCESS)
    {
        /* Enable message buffer interrupt*/
        result = FLEXCAN_SetMsgBuffIntCmd(base, mb_idx, true);
    }
#endif

    return result;
}


#if FEATURE_CAN_HAS_PRETENDED_NETWORKING

#if 0
/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_WakeUpHandler
 * Description   : Wake up handler for FLEXCAN.
 * This handler verifies the event which caused the wake up and invokes the
 * user callback, if configured.
 * This is not a public API as it is called whenever an wake up event occurs.
 *
 *END**************************************************************************/
void FLEXCAN_WakeUpHandler(uint8_t instance)
{
    assert(instance < CAN_INSTANCE_COUNT);

    CAN_Type * base = g_can[instance];
    flexcan_state_t * state = g_flexcanStatePtr[instance];


#if FEATURE_CAN_HAS_PRETENDED_NETWORKING
        if (FLEXCAN_IsPNEnabled(base))
        {
            if (FLEXCAN_GetWTOF(base) != 0U)
            {
                FLEXCAN_ClearWTOF(base);
                /* Invoke callback */
                if (state->callback != NULL)
                {
                	state->callback(instance, FLEXCAN_EVENT_WAKEUP_TIMEOUT, 0U, state);
                }
            }
            if (FLEXCAN_GetWUMF(base) != 0U)
            {
                FLEXCAN_ClearWUMF(base);
                /* Invoke callback */
                if (state->callback != NULL)
                {
                	state->callback(instance, FLEXCAN_EVENT_WAKEUP_MATCH, 0U, state);
                }
            }
        }
#endif
}

#endif





/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_GetWMB
 * Description   : Extracts one of the frames which triggered the wake up event.
 *
 * Implements    : FLEXCAN_DRV_GetWMB_Activity
 *END**************************************************************************/
void FLEXCAN_DRV_GetWMB(uint8_t instance, uint8_t wmbIndex, can_msgbuff_t *wmb)
{

    assert(instance < CAN_INSTANCE_COUNT);
    assert(wmb != NULL);
    const CAN_Type *base  =  g_can[instance];
    uint32_t *tmp, wmbData;
    dbg("%s\n", __FUNCTION__);

    tmp = (uint32_t *)&wmb->data[0];
    wmbData = base->WMB[wmbIndex].WMBn_D03;
    FlexcanSwapBytesInWord(wmbData, *tmp);
    tmp = (uint32_t *)&wmb->data[4];
    wmbData = base->WMB[wmbIndex].WMBn_D47;
    FlexcanSwapBytesInWord(wmbData, *tmp);

    wmb->cs = base->WMB[wmbIndex].WMBn_CS;

    if ((wmb->cs & CAN_CS_IDE_MASK) != 0U)
    {
        wmb->msg_id = base->WMB[wmbIndex].WMBn_ID;
    }
    else
    {
        wmb->msg_id = base->WMB[wmbIndex].WMBn_ID >> CAN_ID_STD_SHIFT;
    }

    wmb->data_len = (uint8_t)((wmb->cs & CAN_CS_DLC_MASK) >> 16);
}
#endif /* FEATURE_CAN_HAS_PRETENDED_NETWORKING */



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_SetBitrateCbt
 * Description   : Set FlexCAN bitrate.
 * This function will set up all the time segment values for the data phase of
 * FD frames. Those time segment values are passed in by the user and are based
 * on the required baudrate.
 *
 * Implements    : FLEXCAN_DRV_SetBitrateCbt_Activity
 *END**************************************************************************/
void FLEXCAN_DRV_SetBitrateFD(uint8_t instance, const flexcan_time_segment_t *bitrate)
{
    DEV_ASSERT(instance < CAN_INSTANCE_COUNT);
    DEV_ASSERT(bitrate != NULL);

    CAN_Type * base = g_can[instance];

    FLEXCAN_EnterFreezeMode(base);

    /* Set time segments*/
    FLEXCAN_SetFDTimeSegments(base, bitrate);

    FLEXCAN_ExitFreezeMode(base);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_DRV_ConfigPN
 * Description   : Configures Pretended Networking settings.
 *
 * Implements    : FLEXCAN_DRV_ConfigPN_Activity
 *END**************************************************************************/
void FLEXCAN_DRV_ConfigPN(uint8_t instance, bool enable, const flexcan_pn_config_t *pnConfig)
{
    assert(instance < CAN_INSTANCE_COUNT);

    CAN_Type * base = g_can[instance];

    FLEXCAN_EnterFreezeMode(base);

    if (enable)
    {
        FLEXCAN_ConfigPN(base, pnConfig);
        FLEXCAN_SetSelfWakeUp(base, false);
    }

    FLEXCAN_SetPN(base, enable);

    FLEXCAN_ExitFreezeMode(base);
}
