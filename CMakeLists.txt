cmake_minimum_required(VERSION 3.14)

set(CMAKE_VERBOSE_MAKEFILE  ON)
set(CMAKE_TOOLCHAIN_FILE  "../arc_toolchain.cmake")

# 生成compile_commands.json
SET(CMAKE_EXPORT_COMPILE_COMMANDS on)

set(app_name "rtt_arc32")
project(${app_name} C CXX ASM)
set(CMAKE_C_STANDARD 99)
set (CMAKE_CXX_STANDARD 11)

#add_definitions(-DRT_USING_DEVICE)
add_definitions(-D_HAVE_LIBGLOSS_)

message("project rood dir:" "${PROJECT_SOURCE_DIR}")
message(${CMAKE_TOOLCHAIN_FILE})
message("${CMAKE_C_COMPILER}")

set(srcs
        bsp/taurus/arc/startup/arc_startup.s
        bsp/taurus/arc/arc_exc_asm.S
        bsp/taurus/arc/startup/arc_cxx_support.c
        bsp/taurus/arc/arc_cache.c
        bsp/taurus/arc/arc_connect.c
        bsp/taurus/arc/arc_mpu.c
        bsp/taurus/arc/arc_timer.c
        bsp/taurus/arc/arc_udma.c
        bsp/taurus/arc/arc_util.s
        bsp/taurus/arc/arc_exception.c
        libcpu/arc/em/cpuport.c
        libcpu/arc/em/contex_gcc_mw.S
        os/clock.c
        os/components.c
        os/cpu.c
        os/idle.c
        os/ipc.c
        os/irq.c
        os/kservice.c
        os/mem.c
        os/memheap.c
        os/mempool.c
        os/object.c
        os/scheduler.c
        os/slab.c
        os/thread.c
        os/timer.c
        os/device.c
        bsp/taurus/board/board.c
#        bsp/taurus/app/printf.c
        bsp/taurus/board/console_io.c
        bsp/taurus/drivers/uart/dw_uart.c
        bsp/taurus/drivers/uart/dw_uart_obj.c
        bsp/taurus/drivers/gpio/dw_gpio.c
        bsp/taurus/drivers/gpio/dw_gpio_obj.c
#        bsp/taurus/clib/embARC_sbrk.c
#        bsp/taurus/clib/embARC_syscalls.c
        components/drivers/serial/serial.c
        components/drivers/src/completion.c
        components/drivers/src/ringbuffer.c
        components/finsh/cmd.c
        components/finsh/msh.c
        components/finsh/shell.c
        components/utilities/ulog/ulog.c
        components/utilities/ulog/backend/console_be.c
        components/net/at/src/at_base_cmd.c
        components/net/at/src/at_cli.c
        components/net/at/src/at_server.c
        components/net/at/src/at_utils.c
        bsp/taurus/clib/embARC_sbrk.c
        bsp/taurus/app/main.c
        bsp/taurus/board/rt_board.c
    )

  set(elf_file       ${app_name}.elf)

  add_executable(${elf_file} ${srcs} )
  target_include_directories(${elf_file} PUBLIC

          ${PROJECT_SOURCE_DIR}/
          ${PROJECT_SOURCE_DIR}/include/
          bsp/taurus/app/
          bsp/taurus/arc/
          bsp/taurus/board
          bsp/taurus/configs/
          bsp/taurus/drivers/uart/
          bsp/taurus/drivers/gpio/
          bsp/taurus/drivers/spiflash/
          bsp/taurus/drivers/spi/
          bsp/taurus/drivers/inc/
          bsp/taurus/inc/
          bsp/taurus/inc/arc/
          bsp/taurus/
          bsp/
          components/drivers/include/
          components/drivers/include/ipc/
#          components/drivers/include/drivers/
          components/finsh/
          components/utilities/ulog/
          components/net/at/include

    )

#set(COMPILE_FLAGS  "--coverage" )
#set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} --coverage")

  #linker file
if(TOOLCHAIN_MWDT)
    set_target_properties(${elf_file} PROPERTIES LINK_FLAGS "${PROJECT_SOURCE_DIR}/arc.ld  -Hnocrt -Hldopt=-q")
elseif(TOOLCHAIN_GNU)
#        set(CMAKE_EXE_LINKER_FLAGS "-Wl,-gc-sections --specs=nano.specs -nostdlib -static -nostartfiles")
    set(CMAKE_EXE_LINKER_FLAGS "-Wl,-gc-sections --specs=nosys.specs  -u _printf_float -static -nostartfiles")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS} -Wl,--script=${PROJECT_SOURCE_DIR}/bsp/taurus/configs/arc_gnu.ld \
    -Wl,-M,-Map=${PROJECT_BINARY_DIR}/${app_name}.map")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -Wl,--script=${PROJECT_SOURCE_DIR}/bsp/taurus/configs/arc_gnu.ld \
    -Wl,-M,-Map=${PROJECT_BINARY_DIR}/${app_name}.map")
    target_link_libraries(${elf_file} -Wl,--start-group)
    target_link_libraries(${elf_file} m gcc c)
    target_link_libraries(${elf_file} -Wl,--end-group)
endif()

add_custom_command(TARGET ${elf_file} POST_BUILD COMMAND ${SIZE}  ${elf_file})
